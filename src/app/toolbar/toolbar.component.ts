import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  @Output() navigate = new EventEmitter<string>();
  modalOpen = false;

  constructor() { }

  ngOnInit(): void {
  }

  openContactModal() {
    this.modalOpen = true;
  }

  closeContactModal() {
    this.modalOpen = false;
  }

  onNavClick(where: string) {
    this.navigate.emit(where);
  }
}
