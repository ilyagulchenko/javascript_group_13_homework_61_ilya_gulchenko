import {Component} from '@angular/core';

@Component({
  selector: 'app-recall',
  template: `<h1 style="text-align: center; font-weight: bold; margin-top: 50px; margin-bottom: 50px">Спасибо за доверие, наш оператор свяжется с вами в самое ближайшее время!</h1>
  <div class="background"></div>`,
  styles: [`
    .background {
      height: 700px;
      background-image: url("https://www.1zoom.ru/big2/691/242564-MrShadow.jpg");
      background-size: 800px;
      background-repeat: no-repeat;
      background-position: right bottom;
    }
  `]
})

export class RecallComponent {

}
