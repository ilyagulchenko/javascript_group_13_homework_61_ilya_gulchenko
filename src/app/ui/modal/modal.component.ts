import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  @Input() isOpen = false;
  @Output() close = new EventEmitter<void>();
  image = 'https://images6.alphacoders.com/428/428855.jpg';

  constructor(private router: Router) {
  }

  onClose() {
    this.close.emit();
  }

  callBack() {
    void this.router.navigate(['/recall']);
  }
}
